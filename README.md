Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

    * A:  The ConsoleHandler and FileHandler have different levels, with the level for the ConsoleHandler being defined as INFO and
    the level for the FileHandler being defined as ALL. Since the level for the ConsoleHandler is INFO, only the logs with a level
    of INFO are printed to the console, whereas the FileHandler, since it has a level of ALL, prints every single log to the file.

2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

    * A:  This line is a log that can be seen in the file, logger.log, which is handled by the FileHandler. This log is created during a JUnit test
    while the automized tester is reading through each method test to determine which tests are enabled. After finding that a certain test was
    not disabled, the automized tester logs the ConditionEvaluationResult, stating that the test is enabled because "@Disabled" was not present. 

3.  What does Assertions.assertThrows do?

    * A:  This method asserts that the given Executable object, after being run, throws an exception of the given exception Class, and it returns the
    exception. The assertion fails if the executable does not throw the exception, and succeeds if the executable throws the exception. This method
    is one of the many assertion methods in JUnit which determine whether a test fails or succeeds.

4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    2.  Why do we need to override constructors?
    3.  Why we did not override other Exception methods?

    A:
         1. A serialVersionUID is used when objects are serialized and deserialized, which means being converted to and from a series of bytes, and
            this is done to store objects in memory. When desirializing an object of a certain class, having the correct serialVersionUID guarantees
            that the class is of the proper format for desirialization. The JVM can generate these automatically; however, different versions of the
            JVM may result in different identifiers, so explicitly declaring a serialVersionUID allows for objects to be deserialized accross different
            versions of the JVM. (Source: What is serialVersionUID? https://dzone.com/articles/what-is-serialversionuid)
         2. It is only possible to instantiate a class by using the constructors defined in that class. It is impossible to create an object by
            using a constructor from the parent. This is why there are constructors with the exact same parameters as those in the parent class.
            For example, if the constructor, TimeException(String message), did not exist, then the code, "new TimeException("message")", would
            result in an error, even though the Exception class has that constructor, since it is impossible to create objects by using constructors
            from the parent class. The constructors are not automatically inherited from the parent.
         3. The other Exception methods were not overriden because all of these methods are automatically inherited from the parent. The only reason
            to override these methods is to change the functionality, which is not necessary for the TimeException class.


5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

    * A:  The static block executes static initialization code, which is the first code that executes in class, running before any constructors or
    static methods. This static block creates an input stream from the logger.properties file. Then, it uses the input stream to configure the
    log manager using the "readConfiguration" method. If the logger.properties file could not be opened, a message is printed to the console.
    Otherwise, the program logs the message, "starting the app".

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

    * A: The README.md file contains text, a restricted set of HTML tags, and, most importantly, Markdown syntax. BitBucket can display README.md files
    on repository source pages because BitBucket uses the Markdown syntax for formatting text.

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

    * A:  The test is failing because of the failure of the unit test titled "failOverTest". This is due to the timeMe method failing to throw the
    TimeException class when a negative one is inputted. In order for the method to be fixed, timeMe has to successfully throw a TimeException class
    when it receives a negative value.

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

    * A:  The issue is caused by a null reference to timeNow, which occurs due to the following exception handling sequence in this method. First,
    timeNow is set to null. Then, the program enters the try block. Since timeToWait is less than zero, a TimerException is thrown. However, instead
    of completely exiting the method, the program enters the finally block. In this block, the program attempts to dereference the null pointer,
    timeNow, in a subtraction expression, which causes a NullPointerException, rather than a TimerException, to be thrown.

9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)

    ![JUnit Plugin Screen Capture](JUnit_Plugin_Screen_Capture_Samuel_Kim_Lab_8_3471.jpg)

10.  Make a printScreen of your eclipse Maven test run, with console

    ![Maven Test Run With Console Screen Capture](Maven_Test_Run_With_Console_Capture_Samuel_Kim_Lab_8_3471.jpg)

11.  What category of Exceptions is TimerException and what is NullPointerException

    * A: The NullPointerException is unchecked, which means that the compiler does not require this exception to be caught or explicitly indicated in the
    method header as thrown when this exception occurs in a method. The TimerException, on the other hand, is a a checked exception, which means that
    all occurrences of this exception must be either handled by a try/catch block or inside of a method which is explicitly stated to throw that
    exception.

12.  Push the updated/fixed source code to your own repository.